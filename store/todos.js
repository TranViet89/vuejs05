export const state = () => ({
  data :  []
});

export const mutations = {
  get(state, data) {
    state.data = data;
  },
  add(state, todo) {
    state.data.push(todo);
  },
  delete(state, id) {
    const todos = state.data;
    state.data = todos.filter(todo => todo.id != id);
  },
  toggle(state, id) {
    const todos = state.data;
    state.data = todos.map(todo => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed
        }
      }

      return todo;
  
    });
  }
}