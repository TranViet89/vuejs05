import Vue from 'vue'

Vue.filter('to-uppercase', val => val.toUpperCase())
